#!/bin/bash -e

if [ ! -d build ]; then
mkdir build
fi

cd src
find . -type f -name "*.java" -printf "$PWD/%h/%f\n" > ../build/srcfiles.txt
cd ..
javac --release 21 -Xlint:deprecation -encoding utf-8 -d ./build "@build/srcfiles.txt"
